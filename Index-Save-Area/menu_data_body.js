_menuCloseDelay=300           // The time delay for menus to remain visible on mouse out
_menuOpenDelay=30            // The time delay before menus open on mouse over
_subOffsetTop=0              // Sub menu top offset
_subOffsetLeft=0            // Sub menu left offset

/// Style Definitions ///

with(subStyle=new mm_style()){
onbgcolor="yellow";
oncolor="black";
offbgcolor="white";
offcolor="black";
bordercolor="#367E45";
borderstyle="solid";
borderwidth=1;
separatorcolor="#325235";
separatorsize=2;
padding=4;
fontsize=12;
fontstyle="normal";
fontfamily="Verdana, Tahoma, Arial";
subimage="";
onsubimage="";
subimagepadding="0 0 0 10";
headerbgcolor="#548959";
headercolor="#f2f2ff";
pagecolor="black"
}

with(subStyle1=new mm_style()){
onbgcolor="yellow";
oncolor="black";
offbgcolor="white";
offcolor="black";
bordercolor="#367E45";
borderstyle="solid";
borderwidth=1;
separatorcolor="#325235";
separatorsize=2;
padding=4;
fontsize=12;
fontstyle="normal";
fontfamily="Verdana, Tahoma, Arial";
subimage="";
onsubimage="";
subimagepadding="0 0 0 10";
headerbgcolor="#548959";
headercolor="#f2f2ff";
pagecolor="black"
}

with(subStyle2=new mm_style()){
onbgcolor="blue";
oncolor="white";
offbgcolor="yellow";
offcolor="black";
bordercolor="#367E45";
borderstyle="solid";
borderwidth=1;
separatorcolor="#325235";
separatorsize=2;
padding=4;
fontsize=12;
fontstyle="normal";
fontfamily="Verdana, Tahoma, Arial";
subimage="";
onsubimage="";
subimagepadding="0 0 0 10";
headerbgcolor="#548959";
headercolor="#f2f2ff";
pagecolor="black"
}


/// Submenu Definitions ///


/// Home ///

with(milonic=new menuname("Home")){
style=subStyle;
top="offset=-10";
left="offset=+0";

aI("status=Main Page;text=<div align=left><b>Main Page</b></div>;url=body.html;target=body;");
aI("status=Our Location;text=<div align=left><b>Our Location</b></div>;url=maps.html;target=body;");
//aI("status=Statistics;text=<div align=left><b>Statistics</b></div>;url=under-construction.html;target=body;");
}

/// HOA ///

with(milonic=new menuname("BOD")){
style=subStyle;
top="offset=-0";
left="offset=+10";
aI("status=Board of Directors;text=<div align=left><b>Board of Directors</b></div>;url=directors.html;target=body;");
aI("showmenu=BOD Agenda-Minutes;text=<div align=left><b>HOA Board<br>Agenda-Minutes</b></div>;target=body;");
aI("showmenu=BOD Homeowners;text=<div align=left><b>Annual Homeowners<br>Agenda-Minutes</b></div>;target=body;");
aI("showmenu=BOD Budget;text=<div align=left><b>Annual Budget<br>Agenda-Minutes</b></div>;target=body;");
}

with(milonic=new menuname("BOD Agenda-Minutes")){
style=subStyle;
top="offset=-0";
left="offset=+0";
aI("showmenu=BOD Agenda-Minutes 2016;text=<div align=left><b>2016</b></div>;target=body;");
aI("showmenu=BOD Agenda-Minutes 2015;text=<div align=left><b>2015</b></div>;target=body;");
}

with(milonic=new menuname("BOD Agenda-Minutes 2016")){
style=subStyle;
top="offset=-0";
left="offset=+0";
aI("status=Feb 5th;text=<div align=left><b>Feb 5th - Agenda</b></div>;url=Protected/Executive-Board-Meeting-Agenda-2-5-2016.pdf;target=_blank;");
}

with(milonic=new menuname("BOD Agenda-Minutes 2015")){
style=subStyle;
top="offset=-40";
left="offset=+0";
aI("status=Apr 25th;text=<div align=left><b>Apr 25th - Minutes</b></div>;url=Protected/Executive-Board-Meeting-Minutes-4-25-2015.pdf;target=_blank;");
aI("status=May 26th;text=<div align=left><b>May 26th - Minutes</b></div>;url=Protected/Executive-Board-Meeting-Minutes-5-26-2015.pdf;target=_blank;");
aI("status=Jul 9th;text=<div align=left><b>Jul 9th - Minutes</b></div>;url=Protected/Executive-Board-Meeting-Minutes-7-9-2015.pdf;target=_blank;");
aI("status=Aug 13th;text=<div align=left><b>Aug 13th - Minutes</b></div>;url=Protected/Executive-Board-Meeting-Minutes-8-13-2015.pdf;target=_blank;");
aI("status=Sep 10th;text=<div align=left><b>Sep 10th - Minutes</b></div>;url=Protected/Executive-Board-Meeting-Minutes-9-10-2015.pdf;target=_blank;");
}

with(milonic=new menuname("BOD Homeowners")){
style=subStyle;
top="offset=+10";
left="offset=+0";
aI("status=HOA 2015;text=<div align=left><b>2015 - Draft</b></div>;url=Protected/Annual-Meeting-Minutes-4-25-2015.pdf;target=_blank;");
}

with(milonic=new menuname("BOD Budget")){
style=subStyle;
top="offset=+10";
left="offset=+0";
aI("status=2016 Agenda;text=<div align=left><b>2016 Agenda</b></div>;url=Protected/BOD-Budget-Minutes-Agenda-12-19-2015.pdf;target=_blank;");
}

/// Documents ///

with(milonic=new menuname("Documents")){
style=subStyle;
top="offset=-10";
left="offset=+20";

aI("showmenu=Committees;text=<div align=left><b>Committees</b></div>;target=body;");
aI("status=Directory;text=<div align=left><b>Directory</b></div>;url=Protected/Nassau-Grove-HOA-Directory.pdf;target=_blank;");
aI("showmenu=Financial;text=<div align=left><b>Financial</b></div>;target=body;");
aI("status=Governing;text=<div align=left><b>Governing</b></div>;url=governing-documents.html;target=body;");
aI("showmenu=Newsletters;text=<div align=left><b>Newsletters</b></div>;target=body;");
aI("status=Street Captains;text=<div align=left><b>Street Captains</b></div>;url=Protected/Street-Captains-2015.pdf;target=_blank;");
}

/// Committees Docs ///

with(milonic=new menuname("Committees")){
style=subStyle2;
top="offset=-0";
left="offset=+0";

aI("showmenu=ARC;text=<div align=left><b>Architectural Review</b></div>;target=body;");
aI("showmenu=Bylaws;text=<div align=left><b>Bylaws</b></div>;target=body;");
aI("showmenu=Common Grounds;text=<div align=left><b>Common Grounds</b></div>;target=body;");
aI("showmenu=Energy;text=<div align=left><b>Energy</b></div>;target=body;");
aI("showmenu=Finance;text=<div align=left><b>Finance</b></div>;target=body;");
aI("showmenu=Landscape;text=<div align=left><b>Landscape</b></div>;target=body;");
aI("showmenu=Security;text=<div align=left><b>Security</b></div>;target=body;");
aI("showmenu=Social;text=<div align=left><b>Social</b></div>;target=body;");
}

/// ARC Committee ///
with(milonic=new menuname("ARC")){
style=subStyle2;
top="offset=-0";
left="offset=+0";

aI("status=Members;text=<div align=left><b>Members</b></div>;url=Protected/Master-Committee-Members-List.pdf;target=_blank;");

aI("showmenu=ARC Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;target=body;");
//aI("status=ARC Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;url=under-construction.html;target=body;");

aI("showmenu=ARC Guidelines & Forms;text=<div align=left><b>Guidelines & Forms</b></div>;target=body;");
}

with(milonic=new menuname("ARC Agenda & Minutes")){
style=subStyle2;
top="offset=-0";
left="offset=+0";
aI("status=Mar 21, 2016 Agenda;text=<div align=left><b>Mar 21, 2016 Agenda</b></div>;url=Protected/arc-agenda-3-21-2016.pdf;target=_blank;");
aI("status=Apr 18, 2016 Agenda;text=<div align=left><b>Apr 18, 2016 Agenda</b></div>;url=Protected/arc-agenda-4-18-2016.pdf;target=_blank;");
}

with(milonic=new menuname("ARC Guidelines & Forms")){
style=subStyle2;
top="offset=-20";
left="offset=+0";
aI("status=NG ARC Manual;text=<div align=left><b>NG ARC Manual</b></div>;url=Protected/NGARC-Manual-Approved-20Dec2014.pdf;target=_blank;");
aI("status=NG ARC Application;text=<div align=left><b>NG ARC Application</b></div>;url=Protected/NGARC-Application-Rev-20160222.pdf;target=_blank;");
}

/// Bylaws Committee ///

with(milonic=new menuname("Bylaws")){
style=subStyle2;
top="offset=-0";
left="offset=+0";
aI("status=Members;text=<div align=left><b>Members</b></div>;url=Protected/Master-Committee-Members-List.pdf;target=_blank;");
aI("showmenu=Bylaws Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;target=body;");
aI("status=Guidelines & Forms;text=<div align=left><b>Guidelines & Forms</b></div>;url=under-construction.html;target=body;");
}

with(milonic=new menuname("Bylaws Agenda & Minutes")){
style=subStyle2;
top="offset=-30";
left="offset=+0";
aI("status=Jan 7, 2015;text=<div align=left><b>Jan 7, 2015 Agenda</b></div>;url=Protected/Bylaws-Committee-Meeting-Agenda-1-7-2015.pdf;target=_blank;");
aI("status=Mar 11, 2015;text=<div align=left><b>Mar 11, 2015 Agenda</b></div>;url=Protected/Bylaws-Committee-Meeting-Agenda-3-11-2015.pdf;target=_blank;");
aI("status=Mar 11, 2015;text=<div align=left><b>Mar 11, 2015 Minutes</b></div>;url=Protected/Bylaws-Committee-Meeting-Minutes-3-11-2015.pdf;target=_blank;");
aI("status=Dec 3, 2014;text=<div align=left><b>Dec 3, 2014 Minutes</b></div>;url=Protected/Bylaws-Committee-Meeting-Minutes-12-3-2014.pdf;target=_blank;");
}

/// Common Grounds Committee ///

with(milonic=new menuname("Common Grounds")){
style=subStyle2;
top="offset=-0";
left="offset=+0";

aI("status=Members;text=<div align=left><b>Members</b></div>;url=Protected/Master-Committee-Members-List.pdf;target=_blank;");

//aI("showmenu=Common Grounds Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;target=body;");
aI("status=Common Grounds Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;url=common-grounds-agenda-minutes.html;target=body;");

aI("status=Guidelines & Forms;text=<div align=left><b>Guidelines & Forms</b></div>;url=under-construction.html;target=body;");
}

with(milonic=new menuname("Common Grounds Agenda & Minutes")){
style=subStyle2;
top="offset=-80";
left="offset=+0";

aI("status=;text=<div align=center><b>2016</b></div>;");
aI("status=Meeting Schedule;text=<div align=left><b>Meeting Schedule</b></div>;url=Protected/Common-Grounds-Committee-Meetings-Schedule-2016.pdf;target=_blank;");
aI("status=Mar 7, 2016;text=<div align=left><b>Mar 7, 2016 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-3-7-2016.pdf;target=_blank;");

aI("status=;text=<div align=center><b>2015</b></div>;");
aI("status=Apr 13, 2015;text=<div align=left><b>Apr 13, 2015 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-4-13-2015.pdf;target=_blank;");
aI("status=May 11, 2015;text=<div align=left><b>May 11, 2015 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-5-11-2015.pdf;target=_blank;");
aI("status=Aug 17, 2015;text=<div align=left><b>Aug 17, 2015 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-8-17-2015.pdf;target=_blank;");
aI("status=Sep 7, 2015;text=<div align=left><b>Sep 7, 2015 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-9-7-2015.pdf;target=_blank;");

aI("status=;text=<div align=center><b>2013</b></div>;");
aI("status=Apr 9, 2013;text=<div align=left><b>Apr 9, 2013 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-4-9-2013.pdf;target=_blank;");
aI("status=May 13, 2013;text=<div align=left><b>May 13, 2013 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-5-13-2013.pdf;target=_blank;");
aI("status=Jul 15, 2013;text=<div align=left><b>Jul 15, 2013 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-7-15-2013.pdf;target=_blank;");
aI("status=Aug 13, 2013;text=<div align=left><b>Aug 13, 2013 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-8-13-2013.pdf;target=_blank;");
aI("status=Sep 9, 2013;text=<div align=left><b>Sep 9, 2013 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-9-9-2013.pdf;target=_blank;");
aI("status=Oct 14, 2013;text=<div align=left><b>Oct 14, 2013 Minutes</b></div>;url=Protected/Common-Grounds-Committee-Meeting-Minutes-10-14-2013.pdf;target=_blank;");
}

///  Energy Committee ///

with(milonic=new menuname("Energy")){
style=subStyle2;
top="offset=-0";
left="offset=+0";
aI("status=Members;text=<div align=left><b>Members</b></div>;url=Protected/Master-Committee-Members-List.pdf;target=_blank;");
aI("status=Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;url=under-construction.html;target=body;");
aI("status=Guidelines & Forms;text=<div align=left><b>Guidelines & Forms</b></div>;url=under-construction.html;target=body;");
}

/// Finance  Committee  ///

with(milonic=new menuname("Finance")){
style=subStyle2;
top="offset=-0";
left="offset=+0";

aI("status=Members;text=<div align=left><b>Members</b></div>;url=Protected/Master-Committee-Members-List.pdf;target=_blank;");
aI("status=Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;url=under-construction.html;target=body;");
aI("status=Guidelines & Forms;text=<div align=left><b>Guidelines & Forms</b></div>;url=under-construction.html;target=body;");
}

///  Landscape Committee  ///

with(milonic=new menuname("Landscape")){
style=subStyle2;
top="offset=-0";
left="offset=+0";
aI("status=Members;text=<div align=left><b>Members</b></div>;url=Protected/Master-Committee-Members-List.pdf;target=_blank;");
aI("showmenu=Landscape Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;;target=body;");
aI("showmenu=Landscape Guidelines & Forms;text=<div align=left><b>Guidelines & Forms</b></div>;target=body;");
}

with(milonic=new menuname("Landscape Agenda & Minutes")){
style=subStyle2;
top="offset=-20";
left="offset=+0";
aI("status=May 8, 2015;text=<div align=left><b>May 8, 2015 Minutes</b></div>;url=Protected/Landscape-Committee-Meeting-Minutes-5-8-2015.pdf;target=_blank;");
aI("status=Oct 16, 2015;text=<div align=left><b>Oct 16, 2015 Agenda</b></div>;url=Protected/Landscape-Committee-Meeting-Agenda-10-16-2015.pdf;target=_blank;");
}


with(milonic=new menuname("Landscape Guidelines & Forms")){
style=subStyle2;
top="offset=-20";
left="offset=+0";
aI("status=2016 Guidelines;text=<div align=left><b>2016 Guidelines</b></div>;url=Protected/Landscape-Guidelines-2-25-2016.pdf;target=_blank;");
aI("status=2016 Application;text=<div align=left><b>2016 Application</b></div>;url=Protected/Landscape-Application-2-25-2016.pdf;target=_blank;");
}

/// Security Committee ///

with(milonic=new menuname("Security")){
style=subStyle2;
top="offset=-0";
left="offset=+0";
aI("status=Members;text=<div align=left><b>Members</b></div>;url=Protected/Master-Committee-Members-List.pdf;target=_blank;");
aI("status=Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;url=under-construction.html;target=body;");
aI("showmenu=Security Guidelines;text=<div align=left><b>Guidelines</b></div>;target=body;");
}

with(milonic=new menuname("Security Guidelines")){
style=subStyle2;
top="offset=-0";
left="offset=+0";
aI("status=Front Gate Opener Styles;text=<div align=left><b>Front Gate Opener Styles</b></div>;url=Protected/Front-Gate-Opener-Styles.pdf;target=_blank;");
}


///  Social  Committee ///

with(milonic=new menuname("Social")){
style=subStyle2;
top="offset=-20";
left="offset=+0";
aI("status=Members;text=<div align=left><b>Members</b></div>;url=Protected/Master-Committee-Members-List.pdf;target=_blank;");
aI("showmenu=Social Agenda & Minutes;text=<div align=left><b>Agenda & Minutes</b></div>;;target=body;");
aI("showmenu=Social Guidelines & Forms;text=<div align=left><b>Guidelines & Forms</b></div>;target=body;");
}

with(milonic=new menuname("Social Guidelines & Forms")){
style=subStyle2;
top="offset=-0";
left="offset=+0";
aI("status=Rules or Scheduled Events;text=<div align=left><b>Rules or Scheduled Events</b></div>;url=Protected/Rules-and-Regulations-for-Scheduling-Resident-Clubhouse-Events.pdf;target=_blank;");
}

with(milonic=new menuname("Social Agenda & Minutes")){
style=subStyle2;
top="offset=-10";
left="offset=+0";
aI("status=Apr 4th, 2014;text=<div align=left><b>Apr 4, 2014 Minutes</b></div>;url=Protected/Social-Committee-Meeting-Minutes-4-4-2014.pdf;target=_blank;");
aI("status=Oct 28th, 2014;text=<div align=left><b>Oct 28, 2014 Minutes</b></div>;url=Protected/Social-Committee-Meeting-Minutes-10-28-2014.pdf;target=_blank;");
}


/// Financial Docs ///

with(milonic=new menuname("Financial")){
style=subStyle;
top="offset=-0";
left="offset=+0";
aI("showmenu=Audited Statements;text=<div align=left><b>Audited Statements</b></div>;target=body;");
aI("showmenu=Budget;text=<div align=left><b>Budget</b></div>;target=body;");
//aI("showmenu=Monthly;text=<div align=left><b>Monthly</b></div>;target=body;");
//aI("showmenu=Treasurer Reports;text=<div align=left><b>Treasurer Reports</b></div>;target=body;");
//aI("showmenu=Reserve Study;text=<div align=left><b>Reserve Study</b></div>;target=body;");
//aI("showmenu=Archived Reports;text=<div align=left><b>Archived Reports</b></div>;target=body;");
}

with(milonic=new menuname("Audited Statements")){
style=subStyle;
top="offset=-0";
left="offset=+0";
aI("status=2014;text=<div align=left><b>2014</b></div>;url=Protected/Audited-Financial-Statements-2014.pdf;target=_blank;");
}

with(milonic=new menuname("Budget")){
style=subStyle;
top="offset=-0";
left="offset=+0";
aI("status=2016;text=<div align=left><b>2016</b></div>;url=Protected/NG-Budget-2016.pdf;target=_blank;");
}


/// Newsletters Docs ///

with(milonic=new menuname("Newsletters")){
style=subStyle;
top="offset=-0";
left="offset=+0";
aI("showmenu=NL 2016;text=<div align=left><b>2016</b></div>;target=body;");
}

with(milonic=new menuname("NL 2016")){
style=subStyle;
top="offset=-30";
left="offset=+0";
aI("status=Jan;text=<b>January</b>;url=documents/Newsletters/Newsletter-Jan-2016.pdf;target=_blank;");
aI("status=Feb;text=<b>February</b>;url=documents/Newsletters/Newsletter-Feb-2016.pdf;target=_blank;");
aI("status=Mar;text=<b>March</b>;url=documents/Newsletters/Newsletter-Mar-2016.pdf;target=_blank;");
}


/// Community News  ///

with(milonic=new menuname("News")){
style=subStyle;
top="offset=-10";
left="offset=+20";
aI("status=Calendar of Events;text=<div align=left><b>Calendar of Events</b></div>;url=http://events.nassaugrovehoa.org;target=_top;");
aI("showmenu=Pictures;text=<div align=left><b>Pictures</b></div>;target=body;");
aI("status=WM Schedule 2016;text=<div align=left><b>Waste Management<br>Schedule 2016</b></div>;url=waste-management-schedule-2016.html;target=body;");
}

with(milonic=new menuname("Pictures")){
style=subStyle2;
top="offset=-50";
left="offset=+0";
aI("status=Community;text=<div align=left><b>Our Community</b></div>;url=gallery-view-community.html;target=_top;");
aI("status=Pig Roast;text=<div align=left><b>Pig Roast 2015</b></div>;url=gallery-view-pig-roast-2015.html;target=_top;");
aI("status=Cold Plate Supper;text=<div align=left><b>Cold Plate Supper 2015</b></div>;url=gallery-view-cold-plate-supper-2015.html;target=_top;");
aI("status=Nearby;text=<div align=left><b>Nearby</b></div>;url=gallery-view-nearby.html;target=_top;");
aI("status=Chili Night;text=<div align=left><b>Chili Night</b></div>;url=gallery-view-chili-night.html;target=_top;");
aI("status=Winter in Lewes;text=<div align=left><b>Winter in Lewes</b></div>;url=gallery-view-winter-in-lewes.html;target=_top;");
aI("status=Evening of Cabaret;text=<div align=left><b>Evening of Cabaret</b></div>;url=gallery-view-evening-of-cabaret.html;target=_top;");
aI("status=Holiday Party;text=<div align=left><b>Holiday Party 2015</b></div>;url=gallery-view-holiday-party-2015.html;target=_top;");
aI("status=Summer Kickoff;text=<div align=left><b>Summer Kickoff 2015</b></div>;url=gallery-view-summer-kickoff-2015.html;target=_top;");
}


///  Community Forum  ///

with(milonic=new menuname("Forum")){
style=subStyle;
top="offset=-10";
left="offset=+20";
aI("text=<div align=left><b>SMF Forum</b></div>;status=Forum;url=SMF-Notice.html;target=body;");
}

///   end of submenus  ///

drawMenus();



function hFPI(_iFFrame){
	var pFi=parent.frames[_iFFrame]
	
	if(!pFi.getMenuByName)return
	
	for(var _c=0;_c<_cip.length;_c++){
		var _ci=_cip[_c];
		var i=getParentItemByItem(_ci);
		if(i==-1)i=_ci
		if(i+" "!=$u){
			while(i!=-1){
				var I=_mi[i]
				_omni=i
				i=getParentItemByItem(i);
				if(i==_omni||i+" "==$u)i=-1
			}
			menuName=_m[_mi[_omni][0]][1]
			P=parent.frames[1]
			
			PM=P.getMenuByName(menuName)
			
			for(var a=0;a<P._mi.length;a++){
				if(P._mi[a][3]==menuName)
				{
					_iFItem=a;
					pFi._setCPage(pFi._mi[_iFItem],_iFItem);
					setTimeout(function(){pFi.itemOn(_iFItem)},20);
					setTimeout(function(){pFi.itemOff(_iFItem)},50);
				}
			}
		}
	}
}

function highlightFramesParentItems(){
	for(var a=0;a<parent.frames.length;a++)hFPI(a)
}
		
highlightFramesParentItems()