_menuCloseDelay=200;           // The time delay for menus to remain visible on mouse out
_menuOpenDelay=30;            // The time delay before menus open on mouse over
_subOffsetTop=+16;             // Sub menu top offset
_subOffsetLeft=0;            // Sub menu left offset

/// Style Definitions ///

with(mainStyleHoriz=new mm_style()){
bordercolor="#999999";
borderstyle="grove";
borderwidth=0;
fontfamily="Verdana, Tahoma, Arial";
fontsize="12";
fontstyle="bold";
headerbgcolor="white";
headercolor="black";
offbgcolor="";
offcolor="white";
onbgcolor="";
oncolor="white";
outfilter="randomdissolve(duration=0.3)";
overfilter="Fade(duration=0.2);Alpha(opacity=100);Shadow(color=#777777', Direction=135, Strength=0)";
padding=24;
pagebgcolor="";
pagecolor="white";
separatorcolor="#999999";
subimage="";
subimagepadding=2;
}

// Main Menu

with(milonic=new menuname("mainMenuHoriz")){
style=mainStyleHoriz;
top=+0;
left=0;
screenposition="center";
orientation="horizontal";
alwaysvisible=1;

aI("status=Management;text=<div align=left><b><font color=yellow>&nbsp;&nbsp;&nbsp;SeaScape<br>Management</font></b></div>;url=management.html;target=body;");

aI("text=<div align=right><b><u>Home</u></b></div>;showmenu=Home;url=body.html;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");

aI("text=<div align=right><b><u>HOA Board</u></b></div>;showmenu=BOD;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");

aI("text=<div align=right><b><u>Documents</u></b></div>;showmenu=Documents;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");

aI("text=<div align=right><b><u>Community News</u></b></div>;showmenu=News;url=area-information.html;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");
aI("text=<div align=left><b><font color=yellow>Community<br>&nbsp;&nbsp;&nbsp;&nbsp;Forum</font></b></div>;status=Forum;url=SMF-Notice.html;target=body;");
}

drawMenus();

