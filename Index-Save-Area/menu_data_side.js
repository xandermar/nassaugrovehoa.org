_menuCloseDelay=300;           // The time delay for menus to remain visible on mouse out
_menuOpenDelay=30;            // The time delay before menus open on mouse over
_subOffsetTop=0;             // Sub menu top offset
_subOffsetLeft=0;            // Sub menu left offset

/// Style Definitions ///

with(mainStyleVert=new mm_style()){
bordercolor="#999999";
borderstyle="grove";
borderwidth=0;
fontfamily="Verdana, Tahoma, Arial";
fontsize="12";
fontstyle="bold";
headerbgcolor="white";
headercolor="black";
offbgcolor="";
offcolor="navy";
onbgcolor="";
oncolor="blue";
outfilter="randomdissolve(duration=0.3)";
overfilter="Fade(duration=0.2);Alpha(opacity=100);Shadow(color=#777777', Direction=135, Strength=0)";
padding=14;
pagebgcolor="";
pagecolor="white";
separatorcolor="#999999";
subimage="";
subimagepadding=2;
}

// Main

with(milonic=new menuname("mainMenuVert")){
style=mainStyleVert;
top=10;
left=0;
itemwidth=196;
alwaysvisible=1;

aI("text=<div align=right><b><u>Management</u></b></div>;showmenu=Management;url=management.html;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");
aI("text=<div align=right><b><u>Home</u></b></div>;showmenu=Home;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");
aI("text=<div align=right><b><u>Documents</u></b></div>;showmenu=Documents;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");
aI("text=<div align=right><b>Community<br><u>News</u></b></div>;showmenu=News;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");
aI("showmenu=DSWA;text=<div align=right><b>DSWA Non-Standard<br>Household Waste<br><u>Disposal</u></b></div>;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");

aI("text=<div align=right><b>Beach<br><u>Shuttle</u></b></div>;showmenu=Shuttle;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");

aI("text=<div align=right><b>Community<br><u>Forum</u></b></div>;showmenu=Forum;url=SMF-Notice.html;target=body;onfunction=openSubmenu();offfunction=closeSubmenu();");
}


drawMenus();

